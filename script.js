// === Set Elements ===
const background = document.querySelector("body");
const changeColor = document.querySelector("#changeColor");

// === Function ===
changeBackgroundColor = function() {
    const red = Math.floor(Math.random()*256 | 0);
    const blue = Math.floor(Math.random()*256 | 0);
    const green = Math.floor(Math.random()*256 | 0);

    background.style.backgroundColor = `rgb(${red} ${blue} ${green})`;
}

// === Set Event Listener ===
changeColor.addEventListener("click", changeBackgroundColor, false);


// === Initialize ===
changeBackgroundColor();